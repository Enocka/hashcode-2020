import java.io.*;
import java.util.*;

public class SolutionTwo {

    public static int daysForScanning;
    TreeSet<Library> allLibraries = new TreeSet<>();

    public static class BookComparator implements Comparator<Book>{
        //factor book scores
        public int compare(Book a, Book b){
            return a.score - b.score;
        }
    }
//    public static BookComparator bookComparator = new BookComparator();

    public class SortByLibWeight implements Comparator<Library>{
        //factor book scores
        public int compare(Library a, Library b){
            return a.libWeight - b.libWeight;
        }
    }

    public class SortByHighestPossibleScoreSum implements Comparator<Order>{
        public int compare(Order a, Order b){
            return a.orderHighestScore - b.orderHighestScore;
        }
    }

    public class Book implements Comparable<Book>{
        public int index;
        public int score;
        public Book(int index, int score){
            this.index = index;
            this.score = score;
        }

        public int compareTo(Book b){
            if(score==b.score)
                if(this.index == b.index)
                    return 0;
                else if(this.index>b.index)
                    return 1;
                else
                    return -1;
            else if(score>b.score)
                return 1;
            else
                return -1;
        }
    }

    public class Library implements Comparable<Library>{
        public int index;
        public int numerOfBooks;
        public int daysToSignUp;
        public int booksShippedPerDay;
        public PriorityQueue<Book> booksLeftOutPQ = new PriorityQueue<>();
        public ArrayDeque<Book> scannedBooksInOrder;
        public Set<Book> booksScannedSet = new TreeSet<>();
        public Set<Book> booksLeftOutSet = new TreeSet<>();
        public int booksScoreSum = 0;
        public boolean finishedScanning = false;


        //use library weight to determine order of examination
        public int libWeight;
        public Library(int numerOfBooks, int daysToSignUp, int booksShippedPerDay, int libIntValue){
            this.numerOfBooks = numerOfBooks;
            this.daysToSignUp = daysToSignUp;
            this.booksShippedPerDay = booksShippedPerDay;
            this.libWeight = (booksScoreSum/(daysForScanning -  this.daysToSignUp))*(booksLeftOutPQ.size()/this.booksShippedPerDay);
            this.index = libIntValue;
            this.booksLeftOutPQ = new PriorityQueue<Book>(1, new BookComparator());
            this.scannedBooksInOrder = new ArrayDeque<Book>();
        }
        public void calculateLibWeight(){
            this.libWeight = (booksScoreSum/(daysForScanning -  this.daysToSignUp))*this.booksShippedPerDay;
        }
        public int compareTo(Library l){
            if(libWeight==l.libWeight){
                if(this.index == l.index)
                    return 0;
                else if(this.index>l.index)
                    return 1;
                else
                    return -1;
            }
            else if(libWeight>l.libWeight)
                return 1;
            else
                return -1;
        }
        public void setScannedBooks(ArrayDeque<Book> scannedBooksQueue){
            Iterator<Book> bkQueueIterator = scannedBooksQueue.iterator();
            while (bkQueueIterator.hasNext()) {
                Book nextBk = bkQueueIterator.next();
                Book bk = new Book(nextBk.index, nextBk.score);
                this.scannedBooksInOrder.add(bk);
                this.booksScannedSet.add(bk);
            }
        }
        public void setBooksLeftOut(PriorityQueue<Book> booksLeftOutPQ){
            Iterator<Book> bkLeftOutPQIterator = booksLeftOutPQ.iterator();
            while (bkLeftOutPQIterator.hasNext()) {
                Book nextBk = bkLeftOutPQIterator.next();
                Book bk = new Book(nextBk.index, nextBk.score);
                this.booksLeftOutPQ.add(bk);
                booksLeftOutSet.add(bk);
            }
        }
    }
    public class Order implements Comparable<Order> {

        public Order() { }

        public int daysForScanning = 100;
        public int orderHighestScore = 0;
        public int daysPassed;
        public int dayLastLibraryScanStarted;
        public Library lastAddedLibrary;
        public PriorityQueue<Library> librariesPQ = new PriorityQueue<>();
        public ArrayList<Library> librariesIncludedList = new ArrayList<>();
        public Set<Library> librariesIncludedSet = new TreeSet<>();
        public Set<Library> librariesLeftOutSet = new TreeSet<>();

        public Set<Book> booksScannedSet = new TreeSet<>();
        public Set<Book> booksLeftOutSet = new TreeSet<>();
//        TreeSet<Library> allLibraries = new TreeSet<>();
        TreeSet<Book> allBooks = new TreeSet<>();


        public Order(
                TreeSet<Library> allLibs,
                TreeSet<Book> allBks,
                PriorityQueue<Library> libsPQ,
                ArrayList<Library> libsIncludedList,
                Set<Library> libsIncludedSet,
                Set<Library> libsLeftOutSet,
                Set<Book> bksScannedSet,
                Set<Book> bksLeftOutSet
        ) {

            Iterator<Library> libraryIterator = allLibs.iterator();
//            while (libraryIterator.hasNext()) {
//                Library nextLib = libraryIterator.next();
//                Library copiedLib = new Library(nextLib.numerOfBooks, nextLib.daysToSignUp, nextLib.booksShippedPerDay, nextLib.index);
//                this.allLibraries.add(copiedLib);
//            }

            Iterator<Book> bookIterator = allBks.iterator();
            while (bookIterator.hasNext()) {
                Book nextBk = bookIterator.next();
                Book copiedBk = new Book(nextBk.index, nextBk.score);
                this.booksLeftOutPQ.add(copiedBk);
            }

            libraryIterator = libsPQ.iterator();
            while (libraryIterator.hasNext()) {
                Library nextLib = libraryIterator.next();
                Library copiedLib = new Library(nextLib.numerOfBooks, nextLib.daysToSignUp, nextLib.booksShippedPerDay, nextLib.index);
                copiedLib.setBooksLeftOut(nextLib.booksLeftOutPQ);
                this.librariesPQ.add(copiedLib);
                this.librariesLeftOutSet.add(copiedLib);
            }


            libraryIterator = libsIncludedList.iterator();
            while (libraryIterator.hasNext()) {
                Library nextLib = libraryIterator.next();
                Library copiedLib = new Library(nextLib.numerOfBooks, nextLib.daysToSignUp, nextLib.booksShippedPerDay, nextLib.index);
                copiedLib.setScannedBooks(nextLib.scannedBooksInOrder);
                this.librariesIncludedList.add(copiedLib);
                this.librariesIncludedSet.add(copiedLib);
            }

//            libraryIterator = libsIncludedSet.iterator();
//            while (libraryIterator.hasNext()) {
//                Library nextLib = libraryIterator.next();
//                Library copiedLib = new Library(nextLib.numerOfBooks, nextLib.daysToSignUp, nextLib.booksShippedPerDay, nextLib.index);
//                this.librariesIncludedSet.add(copiedLib);
//            }

//            libraryIterator = libsLeftOutSet.iterator();
//            while (libraryIterator.hasNext()) {
//                Library nextLib = libraryIterator.next();
//                Library copiedLib = new Library(nextLib.numerOfBooks, nextLib.daysToSignUp, nextLib.booksShippedPerDay, nextLib.index);
//                this.librariesLeftOutSet.add(copiedLib);
//            }

            bookIterator = bksScannedSet.iterator();
            while (bookIterator.hasNext()) {
                Book nextBk = bookIterator.next();
                Book copiedBk = new Book(nextBk.index, nextBk.score);
                this.booksScannedSet.add(copiedBk);
            }

//            bookIterator = bksLeftOutSet.iterator();
//            while (bookIterator.hasNext()) {
//                Book nextBk = bookIterator.next();
//                Book copiedBk = new Book(nextBk.index, nextBk.score);
//                this.booksLeftOutSet.add(copiedBk);
//            }
        }

//        public void setAllLibraries(TreeSet<Library> allLibraries) throws IOException {
//            Iterator<Library> allLibrariesIterator = allLibraries.iterator();
////        System.out.println("allLibraries imepassiwa ikona length ya "+allLibraries.size());
////        System.out.println("allLibraries ziaddiwe kwa this.allLibraries (length 0). Length confirmation :: "+this.allLibraries.size());
//            while (allLibrariesIterator.hasNext()) {
//                Library nextLib = allLibrariesIterator.next();
//                Library lib = new Library(nextLib.numerOfBooks, nextLib.daysToSignUp, nextLib.booksShippedPerDay, nextLib.index);
////            System.out.println("tunaadd library kwa this.allLibraries");
//                this.allLibraries.add(lib);
////            System.out.println("this.allLibraries length imekuwa "+this.allLibraries.size());
//                librariesLeftOutSet.add(lib);
//            }
////        this.allLibraries = allLibraries;
////        librariesLeftOutSet = allLibraries;
//            librariesLeftOutSet.removeAll(librariesIncludedSet);
//        }

        public void setAllBooks(TreeSet<Book> allBooks) throws IOException {
            Iterator<Book> allBooksIterator = allBooks.iterator();
//        System.out.println("allBooks imepassiwa ikona length ya "+allBooks.size());
//        System.out.println("allBooks ziaddiwe kwa this.booksLeftOutPQ (length 0). Length confirmation :: "+this.booksLeftOutPQ.size());
            while (allBooksIterator.hasNext()) {
                Book nextBk = allBooksIterator.next();
                Book bk = new Book(nextBk.index, nextBk.score);
//            System.out.println("tunaadd library kwa this.booksLeftOutPQ");
                this.booksLeftOutPQ.add(bk);
//            System.out.println("this.booksLeftOutPQ length imekuwa "+this.booksLeftOutPQ.size());
                booksLeftOutSet.add(bk);
            }
        }


        public Queue<Order> neighbors() {
            Queue<Order> neighbours = new ArrayDeque<>();
            Iterator<Library> librariesLeftOutSetIterator = librariesLeftOutSet.iterator();
            Iterator<Library> librariesIncludedSetIterator = librariesIncludedSet.iterator();
            Iterator<Book> booksLeftOutSetIterator = booksLeftOutSet.iterator();
//        System.out.println("allLibraries size "+allLibraries.size());
//        System.out.println("librariesIncludedSet size "+librariesIncludedSet.size());
//        System.out.println("librariesLeftOutSet size "+librariesLeftOutSet.size());
            System.out.println();
            System.out.println("*********NEIGHBOURS**********");

//        multiply daysPassed * number of libraries and, for each library, add a book
//        int numberOfPossiblePaths = Solution.daysForScanning - this.daysPassed;
            //why is it better to do it this way, and not librariesLeftOutSetIterator.size() ? madeep copy?
            int numberOfPossiblePaths = allLibraries.size() - librariesIncludedList.size();
            System.out.println("librariesIncludedList inafaa kuwa " + librariesIncludedList.size() + " na this.allLibraries.size inafaa kuwa " + allLibraries.size());
            while (numberOfPossiblePaths > 0) {
                // hapa ndio stuff
//            while(librariesLeftOutSetIterator.hasNext()){
                System.out.println("librariesIncludedSet yenye inatumiwa kutengeneza ikkona size "+this.librariesIncludedSet.size());
                System.out.println("   ******ORDER**********");
                Order newOrder = new Order(allLibraries, this.booksLeftOutPQ, this.librariesPQ, this.librariesIncludedList, this.librariesIncludedSet, this.librariesLeftOutSet, this.booksScannedSet, this.booksLeftOutSet);
                //chek whether a deep copy is needed here !!!!!!!!!!
                Iterator<Library> libIt = this.librariesIncludedSet.iterator();
                while (libIt.hasNext()) {
                    newOrder.librariesIncludedSet.add(libIt.next());
                }
                libIt = this.librariesLeftOutSet.iterator();
                while (libIt.hasNext()) {
                    newOrder.librariesLeftOutSet.add(libIt.next());
                }
                Iterator<Book> bkIt = this.booksScannedSet.iterator();
                while (bkIt.hasNext()) {
                    newOrder.booksScannedSet.add(bkIt.next());
                }
                bkIt = this.booksLeftOutSet.iterator();
                while (bkIt.hasNext()) {
                    newOrder.booksLeftOutSet.add(bkIt.next());
                }
                newOrder.booksScannedSet = this.booksScannedSet;
//                System.out.println("librariesLeftOutSet size before next ya order "+this.toString()+" ni ::: "+librariesLeftOutSet.size());
                Book nextBook;
                /*
                get best library to scan the book
                * */
                Iterator<Library> librariesPQIterator = librariesPQ.iterator();
                //ineficient. find a way of getting any lib that is in the set without removing it
                Iterator<Library> it = this.librariesLeftOutSet.iterator();
                while (it.hasNext()){
                    Library nextLib = it.next();
                    while (librariesPQIterator.hasNext()) {
                        Library lib = librariesPQIterator.next();
                        if (librariesLeftOutSet.contains(lib)) {
                            nextLib = lib;
                        }
                    }

                    for (int i = 0; i < nextLib.daysToSignUp; i++) {
                        //we wont add books to the last library, hence the -1.
                        for (int j = 0; j < newOrder.librariesIncludedList.size() - 1; j++) {
                            Library lib = newOrder.librariesIncludedList.get(j);
                            int indexOfLibInList = 0;
                            //hapa ndio stuff stuff
                            for (int k = 0; k < librariesIncludedList.size(); k++) {
                                Library lb = librariesIncludedList.get(i);
                                if (lb.index == lib.index)
                                    indexOfLibInList = i;
                            }
                            for (int k = 0; k < lib.booksShippedPerDay; ) {
                                nextBook = booksLeftOutSetIterator.next();
                                if (!lib.booksScannedSet.contains(nextBook)) {
                                    lib.booksScannedSet.add(nextBook);
                                    lib.scannedBooksInOrder.add(nextBook);
//                                this.booksScannedSet.add(nextBook);
                                    newOrder.booksScannedSet.add(nextBook);
                                    librariesIncludedList.get(indexOfLibInList).booksScannedSet.add(nextBook);
                                    newOrder.booksLeftOutSet.remove(nextBook);
                                    lib.booksLeftOutSet.remove(nextBook);
                                    librariesIncludedList.get(indexOfLibInList).booksLeftOutSet.remove(nextBook);
//                                this.booksLeftOutSet.remove(nextBook);
                                    lib.booksScoreSum += nextBook.score;
                                    k++;
                                }
                            }
                            boolean bookAdded = false;
                        }
                    }
                    System.out.println("newOrder ikona libs " + newOrder.librariesIncludedList.size());
                    newOrder.librariesIncludedList.add(nextLib);
                    System.out.println("na sasa newOrder "+ newOrder +" ikona libs " + newOrder.librariesIncludedList.size());
                    newOrder.librariesIncludedSet.add(nextLib);
                    newOrder.librariesLeftOutSet.remove(nextLib);
                    newOrder.calculateHighestScore();
//                System.out.println("currentOrder.orderHighestScore "+currentOrder.orderHighestScore);
                    newOrder.daysPassed = daysPassed + nextLib.daysToSignUp;
                    System.out.println("**********ORDER******   ");
                    neighbours.add(newOrder);
                }

                //nini hapa ???
//                nextBook = booksLeftOutSetIterator.next();

                //An extra day has passed

//                System.out.println("currentOrder.daysPassed imesetiwa "+currentOrder.daysPassed);
//                System.out.println("nextLib.daysToSignUp "+nextLib.daysToSignUp);

//            }
                numberOfPossiblePaths--;
            }
            System.out.println("ikona neiighbours " + neighbours.size());
            return neighbours;
        }

        public int compareTo(Order o) {
            if (orderHighestScore == o.orderHighestScore)
                return 0;
            else if (orderHighestScore > o.orderHighestScore)
                return 1;
            else
                return -1;
        }

        public void calculateHighestScore() {
//        System.out.println();
//        System.out.println();
//        System.out.println("calculate highest score inacalliwa");
            int score = 0;
            Set<Library> readyToProcess = new TreeSet<>();
            Iterator<Library> librariesIncludedIterator = librariesIncludedList.iterator();
            int scanDaysPassed = 0;
            Set<Book> scannedBooks = new TreeSet<>();
            Set<Library> readyLibraries = new TreeSet<>();
//        System.out.println("Libraries zitacontribute kwa score ziko "+librariesIncludedList.size());
//        System.out.println("books left out "+booksLeftOutSet.size());
//        System.out.println("books zitacontribute kwa score ziko "+booksScannedSet.size());
            while (librariesIncludedIterator.hasNext()) {
                Library readyLibrary = librariesIncludedIterator.next();
                score += readyLibrary.booksScoreSum;
                orderHighestScore = score;
            }
        }
    }

        public static void main(String[] args) {
            try {
                SolutionTwo obj = new SolutionTwo();
//                obj.run(args);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

// instance variables here

        public void run(String[] args) throws Exception {

            File file = new File("/home/lobi/IdeaProjects/I/src/a_example.txt");
            Scanner input = new Scanner(file);
            int bookCount = input.nextInt();
            int libCount = input.nextInt();
            int daysForScanning = input.nextInt();

            int[] bookScores = new int[bookCount];

            //we need bookCopies so as to
            int[] bookCopies = new int[bookCount];
            for (int i = 0; i < bookCount; i++) {
                bookScores[i] = input.nextInt();
            }

            ArrayList<Library> libraries = new ArrayList<Library>(libCount);
            TreeSet<Library> allLibrariesSet = new TreeSet<Library>();
            TreeSet<Book> allBooksSet = new TreeSet<>();
            PriorityQueue<Order> orderPQ = new PriorityQueue<Order>();
            ArrayList<Order> startingOrders = new ArrayList<Order>(libCount);


            //process first library
            for (int i = 0; i < libCount; i++) {
                int numberOfBooksinLibrary = input.nextInt();
                int daysToSignUp = input.nextInt();
                int booksShippedPerDay = input.nextInt();
//            System.out.println("creating library ... ");
                Library currentLib = new Library(numberOfBooksinLibrary, daysToSignUp, booksShippedPerDay, i);
                Order order = new Order();
                int booksSum = 0;
                for (int j = 0; j < currentLib.numerOfBooks; j++) {
                    int bookScore = input.nextInt();
                    Book book = new Book(j, bookScore);
                    currentLib.booksLeftOutPQ.add(book);
                    currentLib.booksScoreSum += book.score;
                    currentLib.booksLeftOutSet.add(book);
                    order.booksLeftOutSet.add(new Book(j, bookScore));
                }
                currentLib.calculateLibWeight();

                System.out.println("currentLib :: -->> " + currentLib + " na booksLeftOutSet ikona size " + currentLib.booksLeftOutSet.size());
                libraries.add(currentLib);
                allLibraries.add(currentLib);
                allLibrariesSet.add(currentLib);
                Iterator<Book> bkIt = currentLib.booksLeftOutPQ.iterator();
                bkIt.forEachRemaining(book -> {
                    allBooksSet.add(new Book(book.index, book.score));
                });

//            System.out.println("Adding librari "+ i);
//            System.out.println("allLibrariesSet size is "+allLibrariesSet.size());
//            System.out.println("libraries ArrayList size is "+libraries.size());

                //

//            order.librariesIncludedList.add(currentLib);
//            order.librariesIncludedSet.add(currentLib);
                order.booksLeftOutSet.addAll(currentLib.booksLeftOutPQ);

                Iterator<Book> booksLeftOutSetIterator = allBooksSet.iterator();
                Book nextBook;


                //Preparing the first orders
//            Solution.Library lib = currentLib;
                int indexOfLibInList = 0;
                for (int k = 0; k < currentLib.booksShippedPerDay; ) {
                    nextBook = booksLeftOutSetIterator.next();
                    currentLib.booksScannedSet.add(nextBook);
                    currentLib.scannedBooksInOrder.add(nextBook);
                    currentLib.booksLeftOutSet.remove(nextBook);
                    currentLib.booksScoreSum += nextBook.score;
                    order.booksScannedSet.add(nextBook);
//                order.librariesIncludedList.add(currentLib);
                    order.booksLeftOutSet.remove(nextBook);
                    order.librariesLeftOutSet.remove(currentLib);

                    Library listLib = currentLib;

                    k++;
                }

                int score = 0;
                Iterator<Library> librariesIncludedIterator = order.librariesIncludedList.iterator();
//            System.out.println("Libraries zitacontribute kwa score ziko "+order.librariesIncludedList.size());
//            System.out.println("books left out "+order.booksLeftOutSet.size());
//            System.out.println("books zitacontribute kwa score ziko "+order.booksScannedSet.size());
                while (librariesIncludedIterator.hasNext()) {
                    Library readyLibrary = librariesIncludedIterator.next();
                    score += readyLibrary.booksScoreSum;
                }
                order.orderHighestScore = score;

                order.librariesIncludedList.add(currentLib);
                order.librariesIncludedSet.add(currentLib);


                //End of first orders Preparions

                startingOrders.add(order);

//            System.out.println();
//            System.out.println("kwa nini allLibraries set iko empty "+allLibrariesSet.size());
//                order.setAllLibraries(allLibrariesSet);
                order.setAllBooks(allBooksSet);
                order.librariesLeftOutSet.remove(currentLib);
                order.calculateHighestScore();
//            System.out.println("order.orderHighestScore "+order.orderHighestScore);
//            System.out.println("Setting allLibraries in order " + order.toString() + " to size "+ order.allLibraries.size());
//                System.out.println("Adding order "+order);
                orderPQ.add(order);
            }

//        System.out.println();
//        System.out.println("allLibrariesSet size is "+allLibrariesSet.size());
//        System.out.println();

            Iterator<Order> startingOrdersIterator = startingOrders.iterator();


//        set first last added library
            Order currentOrder;
            Order bestOrder = new Order();
            boolean tushafanyaFirsOrder = false;
//        System.out.println("tunataka kujua about bestOrder");
//        System.out.println("na ordersPQ iko na length ya "+orderPQ.size());
            int orderCount = 0;
            while (orderPQ.size() > 0) {

                currentOrder = orderPQ.remove();
                System.out.println("order ya " + ++orderCount +" " +currentOrder);
                System.out.println("ikona libs " + currentOrder.librariesIncludedList.size());
//                System.out.println();
//                System.out.println("after kutoa order moja, ordersPQ iko na length ya "+orderPQ.size());
                if (currentOrder.orderHighestScore > bestOrder.orderHighestScore) {
                    bestOrder = currentOrder;
//                    System.out.println("na kama high score yake imepita tuadjust bestOrder");
                }
//            System.out.println("Creating order neighbours ...");
                Queue<Order> neighbors = currentOrder.neighbors();
//            System.out.println("Created "+ neighbors.size() +" neighbours");
                for (Order neighbor : neighbors) {
//                System.out.println(neighbor.toString());
                    orderPQ.add(neighbor);
//                System.out.println("Adding neighbour ... ");
                }
                System.out.println();
//            System.out.println("after adding neightbours, ordersPQ iko na length ya "+orderPQ.size());
//            System.out.println("so now ordersPQ iko na length ya "+orderPQ.size());
            }

            //now the submission
            int numberOfLibsScanned = bestOrder.librariesIncludedList.size();

            //Print number of books scanned
//        System.out.println("numberOfLibsScanned ::: "+numberOfLibsScanned);
            Iterator<Library> librariesIncludedQueueIterator = bestOrder.librariesIncludedList.iterator();
            System.out.println("number of libraries in the order is " + bestOrder.librariesIncludedList.size());
            while (librariesIncludedQueueIterator.hasNext()) {
                Library l = librariesIncludedQueueIterator.next();
                int numberOfBooksScanned = l.numerOfBooks;
                //Print library, and number of books scanned
                System.out.println(l.index + " " + numberOfBooksScanned);
                System.out.println(l.index + " " + l.scannedBooksInOrder.size());
                Iterator<Book> bookIterator = l.scannedBooksInOrder.iterator();
                bookIterator.forEachRemaining(book -> System.out.print(book.index + " "));
                while (bookIterator.hasNext()) {
                    //print books scanned
//                System.out.print(bookIterator.next().index+" ");
                }
            }
        }
    }











//        int daysLeft = this.daysForScanning;
//        int libsSignedUpForScanning = 0;
//        ArrayList<Integer> librariesSignedUp = new ArrayList<>();
//        Iterator<Library> libraryIterator = libraries.iterator();
//        for(int i = 0; i < this.daysForScanning; ){
//            Library nextLib = libraryIterator.next();
//            libsSignedUpForScanning++;
//            i = i + nextLib.daysToSignUp;
//            int daysLeftToScan = this.daysForScanning - nextLib.daysToSignUp;
//            Set<Integer> scannedBooksInThisLib = new TreeSet<Integer>();
//            for(int j = 0; j < daysLeftToScan; j++){
//                scannedBooksInThisLib.add(nextLib.books.first());
//            }
//            libBooks.put(nextLib.libIntValue, scannedBooksInThisLib);
//        }






//        System.out.println("Number of libraries"+libBooks.size());
//        for(int i = 0; i < libBooks.size(); i++){
//            int libIndex = Integer.parseInt(libBooks.get(i).toString());
//            Set setOfBooksScannedInLibIndex = libBooks.get(i);
//            ArrayList<Integer> libBooksScanned = new ArrayList<>();
//            Iterator<Integer> booksIterator = libBooks.get(i).iterator();
//            while(booksIterator.hasNext()) {
//                libBooksScanned.add(booksIterator.next());
//            }
//            System.out.print("First Library"+libIndex);
//            System.out.println("Number of books in lib"+libBooksScanned.size());
//            for(int j=0; j < libBooksScanned.size(); j++){
//                System.out.print("book "+libBooksScanned.get(i));
//            }
//        }