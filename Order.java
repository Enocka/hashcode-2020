import java.io.*;
import java.util.*;
import java.util.ArrayDeque;
import java.util.Queue;

public class Order implements Comparable<Order>{
    public int daysForScanning = 100;
    public int orderHighestScore = 0;
    public int daysPassed;
    public int dayLastLibraryScanStarted;
    public Solution.Library lastAddedLibrary;
    public PriorityQueue<Solution.Library> libraries = new PriorityQueue<>();
    public ArrayList<Solution.Library> librariesIncludedList = new ArrayList<>();
    public Set<Solution.Library> librariesIncludedSet = new TreeSet<>();
    public Set<Solution.Book> booksScannedSet = new TreeSet<>();

    Set<Solution.Library> librariesLeftOutSet = new TreeSet<>();
    Set<Solution.Book> booksLeftOutSet = new TreeSet<>();
    Set<Solution.Library> allLibraries = new TreeSet<>();
    Set<Solution.Book> allBooks = new TreeSet<>();

    public Order(){ }

    public void setAllLibraries(TreeSet<Solution.Library> allLibraries) throws IOException {
        Iterator<Solution.Library> allLibrariesIterator = allLibraries.iterator();
//        System.out.println("allLibraries imepassiwa ikona length ya "+allLibraries.size());
//        System.out.println("allLibraries ziaddiwe kwa this.allLibraries (length 0). Length confirmation :: "+this.allLibraries.size());
        while(allLibrariesIterator.hasNext()){
            Solution.Library lib = allLibrariesIterator.next();
//            System.out.println("tunaadd library kwa this.allLibraries");
            this.allLibraries.add(lib);
//            System.out.println("this.allLibraries length imekuwa "+this.allLibraries.size());
            librariesLeftOutSet.add(lib);
        }
//        this.allLibraries = allLibraries;
//        librariesLeftOutSet = allLibraries;
        librariesLeftOutSet.removeAll(librariesIncludedSet);
    }

    public void setAllBooks(TreeSet<Solution.Book> allBooks) throws IOException {
        Iterator<Solution.Book> allBooksIterator = allBooks.iterator();
//        System.out.println("allBooks imepassiwa ikona length ya "+allBooks.size());
//        System.out.println("allBooks ziaddiwe kwa this.allBooks (length 0). Length confirmation :: "+this.allBooks.size());
        while(allBooksIterator.hasNext()){
            Solution.Book bk = allBooksIterator.next();
//            System.out.println("tunaadd library kwa this.allBooks");
            this.allBooks.add(bk);
//            System.out.println("this.allBooks length imekuwa "+this.allBooks.size());
            booksLeftOutSet.add(bk);
        }
    }



    public Queue<Order> neighbors(){
        Queue<Order> neighbours = new ArrayDeque<>();
        Iterator<Solution.Library> librariesLeftOutSetIterator = librariesLeftOutSet.iterator();
        Iterator<Solution.Library> librariesIncludedSetIterator = librariesIncludedSet.iterator();
        Iterator<Solution.Book> booksLeftOutSetIterator = booksLeftOutSet.iterator();
//        System.out.println("allLibraries size "+allLibraries.size());
//        System.out.println("librariesIncludedSet size "+librariesIncludedSet.size());
//        System.out.println("librariesLeftOutSet size "+librariesLeftOutSet.size());
//        System.out.println();
//        System.out.println();

//        multiply daysPassed * number of libraries and, for each library, add a book
//        int numberOfPossiblePaths = Solution.daysForScanning - this.daysPassed;
        //why is it better to do it this way, and not librariesLeftOutSetIterator.size() ? madeep copy?
        int numberOfPossiblePaths = this.allLibraries.size() - librariesIncludedSet.size();
        System.out.println("librariesIncludedSet inafaa kuwa "+librariesIncludedSet.size()+" na this.allLibraries.size inafaa kuwa "+this.allLibraries.size());
        while(numberOfPossiblePaths > 0){
            // hapa ndio stuff
//            while(librariesLeftOutSetIterator.hasNext()){
                Order currentOrder = new Order();
                //chek whether a deep copy is needed here !!!!!!!!!!
                Iterator<Solution.Library> libIt = this.librariesIncludedSet.iterator();
                while(libIt.hasNext()){
                    currentOrder.librariesIncludedSet.add(libIt.next());
                }
                libIt = this.librariesLeftOutSet.iterator();
                while(libIt.hasNext()){
                    currentOrder.librariesLeftOutSet.add(libIt.next());
                }
                Iterator<Solution.Book> bkIt= this.booksScannedSet.iterator();
                while(bkIt.hasNext()){
                    currentOrder.booksScannedSet.add(bkIt.next());
                }
                bkIt= this.booksLeftOutSet.iterator();
                while(bkIt.hasNext()){
                    currentOrder.booksLeftOutSet.add(bkIt.next());
                }
                currentOrder.booksScannedSet = this.booksScannedSet;
//                System.out.println("librariesLeftOutSet size before next ya order "+this.toString()+" ni ::: "+librariesLeftOutSet.size());
                Solution.Book nextBook;
                /*
                get best library to scan the book
                * */
                Iterator<Solution.Library> librariesPQIterator = libraries.iterator();
                //ineficient. find a way of getting any lib that is in the set without removing it
                Iterator<Solution.Library> it = this.librariesLeftOutSet.iterator();
                Solution.Library nextLib = it.next();
                while (librariesPQIterator.hasNext()){
                    Solution.Library lib = librariesPQIterator.next();
                    if(librariesLeftOutSet.contains(lib)){
                        nextLib = lib;
                    };
                }

                for(int i = 0; i < nextLib.daysToSignUp; i++){
                    //we wont add books to the last library, hence the -1.
                    for(int j = 0; j < currentOrder.librariesIncludedList.size() - 1; j++){
                        Solution.Library lib = currentOrder.librariesIncludedList.get(j);
                        int indexOfLibInList = 0;
                        //hapa ndio stuff stuff
                        for(int k = 0; k < librariesIncludedList.size(); k++){
                            Solution.Library lb = librariesIncludedList.get(i);
                            if(lb.index == lib.index)
                                indexOfLibInList = i;
                        }
                        for(int k = 0; k < lib.booksShippedPerDay; ) {
                            nextBook = booksLeftOutSetIterator.next();
                            if (!lib.booksScannedSet.contains(nextBook)) {
                                lib.booksScannedSet.add(nextBook);
                                lib.scannedBooksInOrder.add(nextBook);
//                                this.booksScannedSet.add(nextBook);
                                currentOrder.booksScannedSet.add(nextBook);
                                librariesIncludedList.get(indexOfLibInList).booksScannedSet.add(nextBook);
                                currentOrder.booksLeftOutSet.remove(nextBook);
                                lib.booksLeftOutSet.remove(nextBook);
                                librariesIncludedList.get(indexOfLibInList).booksLeftOutSet.remove(nextBook);
//                                this.booksLeftOutSet.remove(nextBook);
                                lib.booksScoreSum += nextBook.score;
                                k++;
                            }
                        }
                        boolean bookAdded = false;
                    }
                }
                System.out.println("currentOrder ikona libs "+currentOrder.librariesIncludedList.size());
                currentOrder.librariesIncludedList.add(nextLib);
                System.out.println("na sasa currentOrder ikona libs "+currentOrder.librariesIncludedList.size());
                currentOrder.librariesIncludedSet.add(nextLib);
                currentOrder.librariesLeftOutSet.remove(nextLib);
                currentOrder.calculateHighestScore();
//                System.out.println("currentOrder.orderHighestScore "+currentOrder.orderHighestScore);
                currentOrder.daysPassed = daysPassed + nextLib.daysToSignUp;
                neighbours.add(currentOrder);
                //nini hapa ???
//                nextBook = booksLeftOutSetIterator.next();

                //An extra day has passed

//                System.out.println("currentOrder.daysPassed imesetiwa "+currentOrder.daysPassed);
//                System.out.println("nextLib.daysToSignUp "+nextLib.daysToSignUp);

//            }
            numberOfPossiblePaths--;
        }
        System.out.println("ikona neiighbours "+neighbours.size());
        return neighbours;
    }

    public int compareTo(Order o){
        if(orderHighestScore == o.orderHighestScore)
            return 0;
        else if(orderHighestScore > o.orderHighestScore)
            return 1;
        else
            return -1;
    }

    public void calculateHighestScore(){
//        System.out.println();
//        System.out.println();
//        System.out.println("calculate highest score inacalliwa");
        int score = 0;
        Set<Solution.Library> readyToProcess = new TreeSet<>();
        Iterator<Solution.Library> librariesIncludedIterator = librariesIncludedList.iterator();
        int scanDaysPassed = 0;
        Set<Solution.Book> scannedBooks = new TreeSet<>();
        Set<Solution.Library> readyLibraries = new TreeSet<>();
//        System.out.println("Libraries zitacontribute kwa score ziko "+librariesIncludedList.size());
//        System.out.println("books left out "+booksLeftOutSet.size());
//        System.out.println("books zitacontribute kwa score ziko "+booksScannedSet.size());
        while(librariesIncludedIterator.hasNext()){
            Solution.Library readyLibrary = librariesIncludedIterator.next();
            score += readyLibrary.booksScoreSum;
//            for(int i = 0; i < daysPassed; i++){
//                if(readyLibrary.booksLeftOutSet.size() == 0)
//                    continue;
//                else {
//                    for(int j = 0; j < readyLibrary.booksShippedPerDay; j++){
//                        Solution.Book nextBookInLibrary = readyLibrary.books.poll();
//                        if(booksScannedSet.contains(nextBookInLibrary))
//                            continue;
//                        else {
//                            booksScannedSet.add(nextBookInLibrary);
//                            score += nextBookInLibrary.score;
//                            readyLibrary.booksScannedSet.add(nextBookInLibrary);
//                            readyLibrary.booksLeftOutSet.remove(nextBookInLibrary);
//                        }
//                    }
//                }
//            }
        }
//        System.out.println();
//        System.out.println("orderHighestScore ikakuwa  == "+this.orderHighestScore);
//        System.out.println();
        orderHighestScore = score;
    }
}


























/*
if(booksLeftOutSetIterator.hasNext()){

//            librariesIncludedQueue;


//            Order currentOrder = new Order();
//            currentOrder.librariesIncludedSet = this.librariesIncludedSet;
//            currentOrder.booksScannedSet = this.booksScannedSet;
//            System.out.println("librariesLeftOutSet size before next ya order "+this.toString()+" ni ::: "+librariesLeftOutSet.size());
//
//            Solution.Book nextBook = booksLeftOutSetIterator.next();
//
//            //An extra day has passed
//            currentOrder.daysPassed = daysPassed++;
//            //
//            if(currentOrder.daysPassed - currentOrder.dayLastLibraryScanStarted == currentOrder.daysForScanning){
//                Solution.Library nextLibrary = librariesLeftOutSetIterator.next();
//                currentOrder.dayLastLibraryScanStarted = currentOrder.daysPassed;
//                currentOrder.lastAddedLibrary = nextLibrary;
//                currentOrder.librariesIncludedList.add(nextLibrary);
//                currentOrder.librariesIncludedSet.add(nextLibrary);
//            }
//            /*
//            get best library to scan the book
//            * */
//            Iterator<Solution.Library> librariesPQIterator = libraries.iterator();
//            while (librariesPQIterator.hasNext()){
//                Solution.Library lib = librariesPQIterator.next();
//                if(lib.booksLeftOutSet.contains(nextBook)){
////                    lib
//                }
//            }
//            // hapa ndio stuff
//            while(librariesIncludedSetIterator.hasNext()){
////                for(int i = 0; i < librariesIncludedList.size(); i++){
//                Solution.Library lib = librariesIncludedSetIterator.next();
//                int indexOfLibInList = 0;
//                if(lib.finishedScanning){
//                    for(int i = 0; i < librariesIncludedList.size(); i++){
//                        Solution.Library lb = librariesIncludedList.get(i);
//                        if(lb.index == lib.index)
//                            indexOfLibInList = i;
//                    }
//                    for(int i = 0; i < lib.booksShippedPerDay; ) {
//                        currentOrder
//                        if (!lib.booksScannedSet.contains(nextBook)) {
//                            lib.booksScannedSet.add(nextBook);
//                            this.booksScannedSet.add(nextBook);
//                            currentOrder.booksScannedSet.add(nextBook);
//                            librariesIncludedList.get(indexOfLibInList).booksScannedSet.add(nextBook);
//                            currentOrder.booksLeftOutSet.remove(nextBook);
//                            lib.booksLeftOutSet.remove(nextBook);
//                            librariesIncludedList.get(indexOfLibInList).booksLeftOutSet.remove(nextBook);
//                            this.booksLeftOutSet.remove(nextBook);
//                            i++;
//                        }
//                    }
//                }
//            }
//            boolean bookAdded = false;
//            currentOrder.calculateHighestScore();
//
//            neighbours.add(currentOrder);
//        }

