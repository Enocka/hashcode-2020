import java.io.*;
import java.util.*;

public class Solution {

    public static int daysForScanning;

    public static class BookComparator implements Comparator<Book>{
        //factor book scores
        public int compare(Book a, Book b){
            return a.score - b.score;
        }
    }
//    public static BookComparator bookComparator = new BookComparator();

    public class SortByLibWeight implements Comparator<Library>{
        //factor book scores
        public int compare(Library a, Library b){
            return a.libWeight - b.libWeight;
        }
    }

    public class SortByHighestPossibleScoreSum implements Comparator<Order>{
        public int compare(Order a, Order b){
            return a.orderHighestScore - b.orderHighestScore;
        }
    }

    public class Book implements Comparable<Book>{
        public int index;
        public int score;
        public Book(int index, int score){
            this.index = index;
            this.score = score;
        }

        public int compareTo(Book b){
            if(score==b.score)
                if(this.index == b.index)
                    return 0;
                else if(this.index>b.index)
                    return 1;
                else
                    return -1;
            else if(score>b.score)
                return 1;
            else
                return -1;
        }
    }

    public class Library implements Comparable<Library>{
        public int index;
        public int numerOfBooks;
        public int daysToSignUp;
        public int booksShippedPerDay;
        public PriorityQueue<Book> books = new PriorityQueue<>();
        public Queue<Book> scannedBooksInOrder;
        public Set<Solution.Book> booksScannedSet = new TreeSet<>();
        public Set<Solution.Book> booksLeftOutSet = new TreeSet<>();
        public  int booksScoreSum = 0;
        public boolean finishedScanning = false;


        //use library weight to determine order of examination
        public int libWeight;
        public Library(int numerOfBooks, int daysToSignUp, int booksShippedPerDay, int libIntValue){
            this.numerOfBooks = numerOfBooks;
            this.daysToSignUp = daysToSignUp;
            this.booksShippedPerDay = booksShippedPerDay;
            this.libWeight = (booksScoreSum/(daysForScanning -  this.daysToSignUp))*(books.size()/this.booksShippedPerDay);
            this.index = libIntValue;
            this.books = new PriorityQueue<Book>(1, new BookComparator());
            this.scannedBooksInOrder = new ArrayDeque<Book>();
        }
        public void calculateLibWeight(){
            this.libWeight = (booksScoreSum/(daysForScanning -  this.daysToSignUp))*this.booksShippedPerDay;
        }
        public int compareTo(Library l){
            if(libWeight==l.libWeight){
                if(this.index == l.index)
                    return 0;
                else if(this.index>l.index)
                    return 1;
                else
                    return -1;
            }
            else if(libWeight>l.libWeight)
                return 1;
            else
                return -1;
        }
    }

    public static void main (String[] args)
    {
        try
        {
            Solution obj = new Solution ();
            obj.run (args);
        }
        catch (Exception e)
        {
            e.printStackTrace ();
        }
    }

// instance variables here

    public void run (String[] args) throws Exception {

        File file = new File("/home/lobi/IdeaProjects/I/src/a_example.txt");
        Scanner input = new Scanner(file);
        int bookCount = input.nextInt();
        int libCount = input.nextInt();
        daysForScanning = input.nextInt();

        int[] bookScores = new int[bookCount];

        //we need bookCopies so as to
        int[] bookCopies = new int[bookCount];
        for(int i = 0; i < bookCount; i++){
            bookScores[i] = input.nextInt();
        }

        ArrayList<Library> libraries = new ArrayList<Library>(libCount);
        TreeSet<Library> allLibrariesSet = new TreeSet<Library>();
        TreeSet<Book> allBooksSet = new TreeSet<>();
        PriorityQueue<Order> orderPQ = new PriorityQueue<Order>();
        ArrayList<Order> startingOrders = new ArrayList<Order>(libCount);


        //process first library
        for(int i = 0; i < libCount; i++){
            int numberOfBooksinLibrary = input.nextInt();
            int daysToSignUp = input.nextInt();
            int booksShippedPerDay = input.nextInt();
//            System.out.println("creating library ... ");
            Library currentLib = new Library(numberOfBooksinLibrary,daysToSignUp,booksShippedPerDay, i);
            Order order = new Order();
            int booksSum = 0;
            for (int j = 0; j < currentLib.numerOfBooks; j++){
                int bookScore = input.nextInt();
                Book book = new Book(j, bookScore);
                currentLib.books.add(book);
                currentLib.booksScoreSum += book.score;
                currentLib.booksLeftOutSet.add(book);
                order.booksLeftOutSet.add(book);
            }
            currentLib.calculateLibWeight();

            System.out.println("currentLib :: -->> "+currentLib+" na booksLeftOutSet ikona size "+currentLib.booksLeftOutSet.size());
            libraries.add(currentLib);
            allLibrariesSet.add(currentLib);
            Iterator<Book> bkIt = currentLib.books.iterator();
            bkIt.forEachRemaining(book -> {
                allBooksSet.add(new Book(book.index, book.score));
            });

//            System.out.println("Adding librari "+ i);
//            System.out.println("allLibrariesSet size is "+allLibrariesSet.size());
//            System.out.println("libraries ArrayList size is "+libraries.size());

            //

//            order.librariesIncludedList.add(currentLib);
//            order.librariesIncludedSet.add(currentLib);
            order.booksLeftOutSet.addAll(currentLib.books);

            Iterator<Solution.Book> booksLeftOutSetIterator = allBooksSet.iterator();
            Book nextBook;



            //Preparing the first orders
//            Solution.Library lib = currentLib;
            int indexOfLibInList = 0;
            for(int k = 0; k < currentLib.booksShippedPerDay; ) {
                nextBook = booksLeftOutSetIterator.next();
                currentLib.booksScannedSet.add(nextBook);
                currentLib.scannedBooksInOrder.add(nextBook);
                currentLib.booksLeftOutSet.remove(nextBook);
                currentLib.booksScoreSum += nextBook.score;
                order.booksScannedSet.add(nextBook);
//                order.librariesIncludedList.add(currentLib);
                order.booksLeftOutSet.remove(nextBook);
                order.librariesLeftOutSet.remove(currentLib);

                Library listLib = currentLib;

                k++;
            }

            int score = 0;
            Iterator<Solution.Library> librariesIncludedIterator = order.librariesIncludedList.iterator();
//            System.out.println("Libraries zitacontribute kwa score ziko "+order.librariesIncludedList.size());
//            System.out.println("books left out "+order.booksLeftOutSet.size());
//            System.out.println("books zitacontribute kwa score ziko "+order.booksScannedSet.size());
            while(librariesIncludedIterator.hasNext()) {
                Solution.Library readyLibrary = librariesIncludedIterator.next();
                score += readyLibrary.booksScoreSum;
            }
            order.orderHighestScore = score;

            order.librariesIncludedList.add(currentLib);
            order.librariesIncludedSet.add(currentLib);


            //End of first orders Preparions

            startingOrders.add(order);

//            System.out.println();
//            System.out.println("kwa nini allLibraries set iko empty "+allLibrariesSet.size());
            order.setAllLibraries(allLibrariesSet);
            order.setAllBooks(allBooksSet);
            order.librariesLeftOutSet.remove(currentLib);
            order.calculateHighestScore();
//            System.out.println("order.orderHighestScore "+order.orderHighestScore);
//            System.out.println("Setting allLibraries in order " + order.toString() + " to size "+ order.allLibraries.size());
            orderPQ.add(order);
        }

//        System.out.println();
//        System.out.println("allLibrariesSet size is "+allLibrariesSet.size());
//        System.out.println();

        Iterator<Order> startingOrdersIterator = startingOrders.iterator();


        // next source of truobleezzzzzzzzzzzz   zz z z z z z z zzz!!


//        while(startingOrdersIterator.hasNext()){
////            System.out.println();
//            Order order = startingOrdersIterator.next();
////            System.out.println("allLibrariesSet.size() ::: "+ allLibrariesSet.size());
//            order.setAllLibraries(allLibrariesSet);
//            order.setAllBooks(allBooksSet);
//            order.calculateHighestScore();
////            System.out.println("order.orderHighestScore "+order.orderHighestScore);
////            System.out.println("Setting allLibraries in order " + order.toString() + " to size "+ order.allLibraries.size());
//            orderPQ.add(order);
//        }

//        System.out.println();
//        System.out.println("priority queue ikona orders iko na length ya "+orderPQ.size());

//        set first last added library
        Order currentOrder;
        Order bestOrder = new Order();
        boolean tushafanyaFirsOrder = false;
//        System.out.println("tunataka kujua about bestOrder");
//        System.out.println("na ordersPQ iko na length ya "+orderPQ.size());
        int orderCount = 0;
        while(orderPQ.size() > 0){
            System.out.println("order ya "+ ++orderCount);
            currentOrder = orderPQ.remove();
            System.out.println("ikona libs "+currentOrder.librariesIncludedList.size());
//            System.out.println();
//            System.out.println("after kutoa order moja, ordersPQ iko na length ya "+orderPQ.size());
            if(currentOrder.orderHighestScore > bestOrder.orderHighestScore){
                bestOrder = currentOrder;
//                    System.out.println("na kama high score yake imepita tuadjust bestOrder");
            }
//            System.out.println("Creating order neighbours ...");
            Queue<Order> neighbors = currentOrder.neighbors();
//            System.out.println("Created "+ neighbors.size() +" neighbours");
            for (Order neighbor : neighbors) {
//                System.out.println(neighbor.toString());
                orderPQ.add(neighbor);
//                System.out.println("Adding neighbour ... ");
            }
//            System.out.println("after adding neightbours, ordersPQ iko na length ya "+orderPQ.size());
//            System.out.println("so now ordersPQ iko na length ya "+orderPQ.size());
        }

        //now the submission
        int numberOfLibsScanned = bestOrder.librariesIncludedList.size();

        //Print number of books scanned
//        System.out.println("numberOfLibsScanned ::: "+numberOfLibsScanned);
        Iterator<Library> librariesIncludedQueueIterator = bestOrder.librariesIncludedList.iterator();
        System.out.println("number of libraries in the order is "+bestOrder.librariesIncludedList.size());
        while (librariesIncludedQueueIterator.hasNext()){
            Library l = librariesIncludedQueueIterator.next();
            int numberOfBooksScanned = l.numerOfBooks;
            //Print library, and number of books scanned
            System.out.println(l.index+" "+numberOfBooksScanned);
            System.out.println(l.index+" "+l.scannedBooksInOrder.size());
            Iterator<Book> bookIterator = l.scannedBooksInOrder.iterator();
            bookIterator.forEachRemaining(book -> System.out.print(book.index+" "));
            while (bookIterator.hasNext()){
                //print books scanned
//                System.out.print(bookIterator.next().index+" ");
            }
        }
    }
}










//        int daysLeft = this.daysForScanning;
//        int libsSignedUpForScanning = 0;
//        ArrayList<Integer> librariesSignedUp = new ArrayList<>();
//        Iterator<Library> libraryIterator = libraries.iterator();
//        for(int i = 0; i < this.daysForScanning; ){
//            Library nextLib = libraryIterator.next();
//            libsSignedUpForScanning++;
//            i = i + nextLib.daysToSignUp;
//            int daysLeftToScan = this.daysForScanning - nextLib.daysToSignUp;
//            Set<Integer> scannedBooksInThisLib = new TreeSet<Integer>();
//            for(int j = 0; j < daysLeftToScan; j++){
//                scannedBooksInThisLib.add(nextLib.books.first());
//            }
//            libBooks.put(nextLib.libIntValue, scannedBooksInThisLib);
//        }






//        System.out.println("Number of libraries"+libBooks.size());
//        for(int i = 0; i < libBooks.size(); i++){
//            int libIndex = Integer.parseInt(libBooks.get(i).toString());
//            Set setOfBooksScannedInLibIndex = libBooks.get(i);
//            ArrayList<Integer> libBooksScanned = new ArrayList<>();
//            Iterator<Integer> booksIterator = libBooks.get(i).iterator();
//            while(booksIterator.hasNext()) {
//                libBooksScanned.add(booksIterator.next());
//            }
//            System.out.print("First Library"+libIndex);
//            System.out.println("Number of books in lib"+libBooksScanned.size());
//            for(int j=0; j < libBooksScanned.size(); j++){
//                System.out.print("book "+libBooksScanned.get(i));
//            }
//        }
